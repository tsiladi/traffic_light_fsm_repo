#define DEBUG true
#define Serial if(DEBUG)Serial
#define SERIAL_BAUDRATE 115200

#define RED_LIGHT 6
#define YELLOW_LIGHT 7
#define GREEN_LIGHT 8

#define RED_LIGHT_DURATION 5000
#define RED_YELLOW_LIGHT_DURATION 1000
#define GREEN_LIGHT_DURATION 5000
#define YELLOW_LIGHT_DURATION 1000


enum trafficLightStates {
    TL_FSM_START,
    TL_FSM_R,
    TL_FSM_R_WAIT,
    TL_FSM_RY,
    TL_FSM_RY_WAIT,
    TL_FSM_G,
    TL_FSM_G_WAIT,
    TL_FSM_Y,
    TL_FSM_Y_WAIT
};

enum trafficLightStates tl_fsm_current_state = TL_FSM_START;
enum trafficLightStates tl_fsm_previous_state = TL_FSM_START;

unsigned long timer_t = 0;
unsigned long timer_t0 = 0;

void trafficLightFSM();

void setup()
{
    pinMode(RED_LIGHT, OUTPUT);
    pinMode(YELLOW_LIGHT, OUTPUT);
    pinMode(GREEN_LIGHT, OUTPUT);

    Serial.begin(SERIAL_BAUDRATE);
    Serial.println("Debugging is ON");
}

void loop()
{
	trafficLightFSM();

}


void trafficLightFSM()
{
    tl_fsm_previous_state =tl_fsm_current_state;
    switch (tl_fsm_current_state)
    {
    case TL_FSM_START:
        /* code */
        /* transition */
        tl_fsm_current_state = TL_FSM_R;
        break;

    case TL_FSM_R:
        /* lights */
        digitalWrite(RED_LIGHT, HIGH);
        digitalWrite(YELLOW_LIGHT, LOW);
        digitalWrite(GREEN_LIGHT, LOW);
        /* transition */
        timer_t0 = millis();
        tl_fsm_current_state = TL_FSM_R_WAIT;
        break;

    case TL_FSM_R_WAIT:
        timer_t = millis();
        if(timer_t - timer_t0 > RED_LIGHT_DURATION) tl_fsm_current_state = TL_FSM_RY;
        break;

    case TL_FSM_RY:
        /* lights */
        digitalWrite(RED_LIGHT, HIGH);
        digitalWrite(YELLOW_LIGHT, HIGH);
        digitalWrite(GREEN_LIGHT, LOW);
        /* transition */
        timer_t0 = millis();
        tl_fsm_current_state = TL_FSM_RY_WAIT;
        break;

    case TL_FSM_RY_WAIT:
        timer_t = millis();
        if(timer_t - timer_t0 > RED_YELLOW_LIGHT_DURATION) tl_fsm_current_state = TL_FSM_G;
        break;

    case TL_FSM_G:
        /* lights */
        digitalWrite(RED_LIGHT, LOW);
        digitalWrite(YELLOW_LIGHT, LOW);
        digitalWrite(GREEN_LIGHT, HIGH);
        /* transition */
        timer_t0 = millis();
        tl_fsm_current_state = TL_FSM_G_WAIT;
        break;
    
    case TL_FSM_G_WAIT:
        timer_t = millis();
        if(timer_t - timer_t0 > GREEN_LIGHT_DURATION) tl_fsm_current_state = TL_FSM_Y;
        break;

    case TL_FSM_Y:
        /* lights */
        digitalWrite(RED_LIGHT, LOW);
        digitalWrite(YELLOW_LIGHT, HIGH);
        digitalWrite(GREEN_LIGHT, LOW);
        /* transition */
        timer_t0 = millis();
        tl_fsm_current_state = TL_FSM_Y_WAIT;
        break;

    case TL_FSM_Y_WAIT:
        timer_t = millis();
        if(timer_t - timer_t0 > YELLOW_LIGHT_DURATION) tl_fsm_current_state = TL_FSM_R;
        break;

    default:
        break;
    }

}